///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//  File:               InfluxDB_Definitions.ttcn
//  Description:
//  Rev:                R1B
//  Prodnr:             CNL 113 909
//  Updated:            2021-02-03
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////
module InfluxDB_Definitions
{
	import from IPL4asp_Types all;
	import from IPL4asp_PortType all;

  ///////////////////////////////////////////////////////////
  //  Module parameter: tsp_InfluxDB_debug
  // 
  //  Purpose:
  //    Enable/disable debug logging
  //
  //  Type:
  //     *boolean*
  //
  //  Default value:
  //     *false*
  ///////////////////////////////////////////////////////////
    modulepar boolean tsp_InfluxDB_debug := false;

  ///////////////////////////////////////////////////////////
  //  Module parameter: tsp_InfluxDB_client_basePort
  // 
  //  Purpose:
  //    Local port number used for the UDP connection with the InfluxDB
  //
  //  Type:
  //     *integer*
  //
  //  Default value:
  //     *9000*
  ///////////////////////////////////////////////////////////
    modulepar integer tsp_InfluxDB_client_basePort := 9000;

  ///////////////////////////////////////////////////////////
  //  Module parameter: tsp_InfluxDB_reportPeriod
  // 
  //  Purpose:
  //    InfluxDB event handler reporting period
  //
  //  Type:
  //     *float*
  //
  //  Default value:
  //     *3.0*
  ///////////////////////////////////////////////////////////
    modulepar float   tsp_InfluxDB_reportPeriod := 3.0;

	type component InfluxDB_CT
	{
      var InfluxDBContext v_InfluxDB_ctx := c_InfluxDBContext_empty;

      var ASP_SendTo 			      v_InfluxDB_sendAsp;
      var charstring			      v_InfluxDB_msgToSend_encoded;
      var InfluxLineProtocol          v_InfluxDB_msgToSend;
      var fcb_InfluxDB_reportHandler  v_InfluxDB_reportHandler := null;
      var default                     v_InfluxDB_eventHandler;  
      timer                           v_InfluxDB_reportTimer;

	  port IPL4asp_PT INFLUXDB_PCO;
	}

	type record InfluxDBContext
	{
		integer connId,
		integer connId_listen,
		charstring remoteHost,
		integer remotePort,
		charstring localHost,
		integer localPort,
		float reportPeriod
	}
	with { extension "done" }

	const InfluxDBContext c_InfluxDBContext_empty :=
	{
		connId := -1,
		connId_listen := -1,
		remoteHost := "127.0.0.1",
		remotePort := 8089,
		localHost := "127.0.0.1",
		localPort := 9000,
		reportPeriod := 3.0
	}
	
	type function fcb_InfluxDB_reportHandler() runs on self;

	type record InfluxLineProtocol
	{
	  charstring 			measurement,
	  InfluxAttrValue_List	tagSet,
	  InfluxAttrValue_List	fieldSet
	}

	const InfluxLineProtocol c_InfluxLineProtocol_empty :=
	{
	  measurement := "",
	  tagSet := {},
	  fieldSet := {}
	}

	type record of InfluxAttrValue InfluxAttrValue_List;

	type record InfluxAttrValue
	{
	  charstring name,
	  charstring val
	}
    
    template InfluxAttrValue t_influxData(in charstring p_key, in charstring p_value) :=
    {
      name := p_key,
      val := p_value
    }
}
